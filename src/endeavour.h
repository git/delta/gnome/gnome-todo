/* endeavour.h
 *
 * Copyright (C) 2015-2020 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ENDEAVOUR_H
#define ENDEAVOUR_H

#include <libpeas/peas.h>

#include "gtd-enum-types.h"

/* THIS FILE MUST BE INCLUDED FIRST, OTHERWISE THE WHOLE APP BREAKS */
#include "gtd-widget.h"

#include "gtd-activatable.h"
#include "gtd-bin-layout.h"
#include "gtd-easing.h"
#include "gtd-keyframe-transition.h"
#include "gtd-list-model-filter.h"
#include "gtd-list-model-sort.h"
#include "gtd-list-store.h"
#include "gtd-manager.h"
#include "gtd-max-size-layout.h"
#include "gtd-menu-button.h"
#include "gtd-notification.h"
#include "gtd-object.h"
#include "gtd-panel.h"
#include "gtd-property-transition.h"
#include "gtd-provider.h"
#include "gtd-provider-popover.h"
#include "gtd-star-widget.h"
#include "gtd-task.h"
#include "gtd-task-list.h"
#include "gtd-task-list-view.h"
#include "gtd-timeline.h"
#include "gtd-transition.h"
#include "gtd-types.h"
#include "gtd-utils.h"
#include "gtd-window.h"
#include "gtd-workspace.h"

#endif /* ENDEAVOUR_H */

